package com.ramon.cursomc.resources;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.ramon.cursomc.domain.PagamentoComBoleto;

@Service
public class BoletoService {

	public void preencherPagamentoComBoleto(PagamentoComBoleto pagmto, Date instateDoPedido){
		Calendar cal = Calendar.getInstance();
		cal.setTime(instateDoPedido);
		cal.add(Calendar.DAY_OF_MONTH, 7);
		pagmto.setDataVencimento(cal.getTime());
	}
}
