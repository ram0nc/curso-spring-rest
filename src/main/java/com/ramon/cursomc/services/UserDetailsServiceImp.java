package com.ramon.cursomc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ramon.cursomc.domain.Cliente;
import com.ramon.cursomc.repository.ClienteRepository;
import com.ramon.cursomc.security.UserSS;

@Service
public class UserDetailsServiceImp implements UserDetailsService{

	@Autowired
	private ClienteRepository repo;
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Cliente cli = repo.findByEmail(email);
		if(cli == null) {
			System.out.println("nao achou o email");
			throw new UsernameNotFoundException(email);
		}
		System.out.println("email top");
		return new UserSS(cli.getId(),cli.getEmail(),cli.getSenha(),cli.getPerfis());
	}

}
