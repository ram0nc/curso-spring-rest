package com.ramon.cursomc.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ramon.cursomc.domain.Cidade;
import com.ramon.cursomc.domain.Cliente;
import com.ramon.cursomc.domain.Endereco;
import com.ramon.cursomc.domain.enums.Perfil;
import com.ramon.cursomc.domain.enums.TipoCliente;
import com.ramon.cursomc.dto.ClienteDTO;
import com.ramon.cursomc.dto.ClienteNewDTO;
import com.ramon.cursomc.repository.ClienteRepository;
import com.ramon.cursomc.repository.EnderecoRepository;
import com.ramon.cursomc.security.UserSS;
import com.ramon.cursomc.services.exceptions.AuthorizationException;
import com.ramon.cursomc.services.exceptions.DataIntegrityException;
import com.ramon.cursomc.services.exceptions.ObjectNotFoundException;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepo;

	@Autowired 
	private EnderecoRepository enderecRepo;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	public Cliente find(Integer id) {
		
		UserSS user = UserService.authenticate();
		if( user == null || !user.hasRole(Perfil.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso Negado!");
		}
		
		Optional<Cliente> cliente = clienteRepo.findById(id);

		return cliente.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Cliente.class.getName()));
	}

	@Transactional
	public Cliente insert(Cliente obj) {
		obj.setId(null);
		obj = clienteRepo.save(obj);
		enderecRepo.saveAll(obj.getEnderecos());
		return obj;
	}

	public Cliente update(Cliente cliente) {

		Cliente newObj = find(cliente.getId());
		updateData(newObj, cliente);
		return clienteRepo.save(newObj);
	}

	public void delete(Integer id) {

		find(id);

		try {
			clienteRepo.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("não é possivel excluir uma cliente que possui entidades relacionadas");
		}

	}

	public List<Cliente> findAll() {
		// TODO Auto-generated method stub
		return clienteRepo.findAll();
	}

	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {

		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return clienteRepo.findAll(pageRequest);
	}

	public Cliente fromDTO(ClienteDTO cliente) {
		return new Cliente(cliente.getId(), cliente.getNome(), cliente.getEmail(), null, null, null);
	}

	private void updateData(Cliente newObj, Cliente cliente) {

		newObj.setEmail(cliente.getEmail());
		newObj.setNome(cliente.getNome());
	}

	
	public Cliente fromDTO(@Valid ClienteNewDTO objDTO) {
		
		Cliente cli = new Cliente(null, objDTO.getNome(), objDTO.getEmail(), objDTO.getCpfOuCnpj(), TipoCliente.toEnum(objDTO.getTipo()),pe.encode(objDTO.getSenha()));
		Cidade cid = new Cidade(objDTO.getCidadeId(), null, null);
		Endereco end = new Endereco(null, objDTO.getLogradouro(), objDTO.getNumero(), objDTO.getComplemento(), objDTO.getBairro(), objDTO.getCep(), cli, cid);
		cli.getEnderecos().add(end);
		cli.getTelefones().add(objDTO.getTelefone1());
		
		if( objDTO.getTelefone2() != null) {
			cli.getTelefones().add(objDTO.getTelefone2());
		}
		
		if( objDTO.getTelefone3() != null) {
			cli.getTelefones().add(objDTO.getTelefone3());
		}
		return cli;
	}

}
