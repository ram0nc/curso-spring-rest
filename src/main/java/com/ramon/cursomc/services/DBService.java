package com.ramon.cursomc.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ramon.cursomc.domain.Categoria;
import com.ramon.cursomc.domain.Cidade;
import com.ramon.cursomc.domain.Cliente;
import com.ramon.cursomc.domain.Endereco;
import com.ramon.cursomc.domain.Estado;
import com.ramon.cursomc.domain.ItemPedido;
import com.ramon.cursomc.domain.Pagamento;
import com.ramon.cursomc.domain.PagamentoComBoleto;
import com.ramon.cursomc.domain.PagamentoComCartao;
import com.ramon.cursomc.domain.Pedido;
import com.ramon.cursomc.domain.Produto;
import com.ramon.cursomc.domain.enums.EstadoPagamento;
import com.ramon.cursomc.domain.enums.Perfil;
import com.ramon.cursomc.domain.enums.TipoCliente;
import com.ramon.cursomc.repository.CategoriaRepository;
import com.ramon.cursomc.repository.CidadeRepository;
import com.ramon.cursomc.repository.ClienteRepository;
import com.ramon.cursomc.repository.EnderecoRepository;
import com.ramon.cursomc.repository.EstadoRepository;
import com.ramon.cursomc.repository.ItemPedidoRepository;
import com.ramon.cursomc.repository.PagamentoRepository;
import com.ramon.cursomc.repository.PedidoRepository;
import com.ramon.cursomc.repository.ProdutoRepository;

@Service
public class DBService {


	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Autowired
	private ItemPedidoRepository itemPedidoRepository;
	
	@Autowired
	private BCryptPasswordEncoder pe; 
	
	public void instantiateTestDatabase() throws ParseException {
		
		Categoria cat1 = new Categoria(null, "Informatica");
		Categoria cat2 = new Categoria(null, "Escritorio");
		Categoria cat3 = new Categoria(null, "Eletronicos");
		Categoria cat4 = new Categoria(null, "Celulares");
		Categoria cat5 = new Categoria(null, "Esportes");
		Categoria cat6 = new Categoria(null, "Bebidas");
		Categoria cat7 = new Categoria(null, "Cama,mesa e banho");


		
		Produto prod1 = new Produto(null, "computador", 2000.00);
		Produto prod2 = new Produto(null, "Impressora", 800.00);
		Produto prod3 = new Produto(null, "mouse", 80.00);
		Produto prod4 = new Produto(null, "Mesa de escritorio", 300.00);
		Produto prod5 = new Produto(null, "Toalha", 50.00);
		Produto prod6 = new Produto(null, "Colcha", 200.00);
		Produto prod7 = new Produto(null, "Tv true color", 1200.00);
		Produto prod8 = new Produto(null, "Roçadeira", 800.00);
		Produto prod9 = new Produto(null, "abajour", 100.00);
		Produto prod10 = new Produto(null, "Pendente", 180.00);
		Produto prod11 = new Produto(null, "Shampoo", 90.00);
		
		cat1.getProdutos().addAll(Arrays.asList(prod1, prod2, prod3));
		cat2.getProdutos().addAll(Arrays.asList(prod2));

		cat2.getProdutos().addAll(Arrays.asList(prod2, prod4));
		cat3.getProdutos().addAll(Arrays.asList(prod5, prod6));
		cat4.getProdutos().addAll(Arrays.asList(prod1, prod2, prod3, prod7));
		cat5.getProdutos().addAll(Arrays.asList(prod8));
		cat6.getProdutos().addAll(Arrays.asList(prod9, prod10));
		cat7.getProdutos().addAll(Arrays.asList(prod11));

		prod1.getCategorias().addAll(Arrays.asList(cat1, cat4));
		prod2.getCategorias().addAll(Arrays.asList(cat1, cat2, cat4));
		prod3.getCategorias().addAll(Arrays.asList(cat1, cat4));
		prod4.getCategorias().addAll(Arrays.asList(cat2));
		prod5.getCategorias().addAll(Arrays.asList(cat3));
		prod6.getCategorias().addAll(Arrays.asList(cat3));
		prod7.getCategorias().addAll(Arrays.asList(cat4));
		prod8.getCategorias().addAll(Arrays.asList(cat5));
		prod9.getCategorias().addAll(Arrays.asList(cat6));
		prod10.getCategorias().addAll(Arrays.asList(cat6));
		prod11.getCategorias().addAll(Arrays.asList(cat7));		

		categoriaRepository.saveAll(Arrays.asList(cat1, cat2, cat3, cat4, cat5, cat6, cat7));
		produtoRepository.saveAll(Arrays.asList(prod1, prod2, prod3, prod4, prod5, prod6, prod7, prod8, prod9, prod10, prod11));
		
		Estado est1 = new Estado(null, "Minas Gerais");
		Estado est2 = new Estado(null, "Sao paulo");
		
		Cidade cid1 = new Cidade(null, "Uberlandia", est1);
		Cidade cid2 = new Cidade(null, "Sao paulo", est2);
		Cidade cid3 = new Cidade(null, "Campinas", est2);
		
		est1.getCidades().addAll(Arrays.asList(cid1));
		est2.getCidades().addAll(Arrays.asList(cid2,cid3));
		
		estadoRepository.saveAll(Arrays.asList(est1,est2));
		cidadeRepository.saveAll(Arrays.asList(cid1,cid2,cid3));
			
		Cliente cli1 = new Cliente(null, "maria silva", "aprovavest2015@gmail.com", "83297581255", TipoCliente.PESSOAFISICA, pe.encode("123"));
		cli1.getTelefones().addAll(Arrays.asList("27363323", "98989696"));
		
		Cliente cli2 = new Cliente(null, "ramon top", "ramoncgusmao@gmail.com", "83297581255", TipoCliente.PESSOAFISICA, pe.encode("123"));
		cli2.getTelefones().addAll(Arrays.asList("959598667", "44554455"));
		cli2.addPerfil(Perfil.ADMIN);

		
		Endereco end1 = new Endereco(null, "Rua flores", "300", "apto 303", "jardim", "38220897", cli1, cid1);
		Endereco end2 = new Endereco(null, "Av matos", "105", "sala 800", "centro", "65022320", cli1, cid2);
		Endereco end3 = new Endereco(null, "Rua viana", "130", "sala 900", "uberaba", "65000320", cli2, cid2);
		
		cli1.getEnderecos().addAll(Arrays.asList(end1,end2));
		cli2.getEnderecos().addAll(Arrays.asList(end3));
		clienteRepository.saveAll(Arrays.asList(cli1,cli2));
		enderecoRepository.saveAll(Arrays.asList(end1,end2,end3));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
		Pedido ped1 = new Pedido(null, sdf.parse("30/09/2017 10:32"),cli1,end1);
		Pedido ped2 = new Pedido(null, sdf.parse("10/10/2017 19:35"),cli1,end2);
		
		Pagamento pagam1 = new PagamentoComCartao(null, EstadoPagamento.QUITADO, ped1, 6);
		ped1.setPagamento(pagam1);
		
		Pagamento pagam2 = new PagamentoComBoleto(null, EstadoPagamento.PENDENTE, ped2, sdf.parse("20/10/2017 00:00"), null);
		ped2.setPagamento(pagam2);
		
		pedidoRepository.saveAll(Arrays.asList(ped1,ped2));
		pagamentoRepository.saveAll(Arrays.asList(pagam1,pagam2));
		
		
		ItemPedido iped1 = new ItemPedido(ped1, prod1, 0.00, 1, 2000.00);
		ItemPedido iped2 = new ItemPedido(ped1, prod3, 0.00, 2, 80.00);
		ItemPedido iped3 = new ItemPedido(ped2, prod2, 100.00, 1, 800.00);
		
		ped1.getItens().addAll(Arrays.asList(iped1,iped2));
		ped2.getItens().addAll(Arrays.asList(iped3));
		
		prod1.getItens().addAll(Arrays.asList(iped1));
		prod2.getItens().addAll(Arrays.asList(iped3));
		prod3.getItens().addAll(Arrays.asList(iped2));
		
		itemPedidoRepository.saveAll(Arrays.asList(iped1,iped2,iped3));
		
		
		
	}
}
