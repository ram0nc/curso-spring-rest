package com.ramon.cursomc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.ramon.cursomc.domain.Categoria;
import com.ramon.cursomc.dto.CategoriaDTO;
import com.ramon.cursomc.repository.CategoriaRepository;
import com.ramon.cursomc.services.exceptions.DataIntegrityException;
import com.ramon.cursomc.services.exceptions.ObjectNotFoundException;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepo;
	
	public Categoria find(Integer id) {
		Optional<Categoria> categoria = categoriaRepo.findById(id);
	
		return categoria.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Categoria.class.getName()));
	}
	
	public Categoria insert(Categoria obj) {
		obj.setId(null);
		return categoriaRepo.save(obj);
	}

	public Categoria update(Categoria categoria) {
		
		Categoria newCat = find(categoria.getId());
		updateData(newCat, categoria);
		return categoriaRepo.save(newCat);
	}

	public void delete(Integer id) {
		
		find(id);
		
		try {
			categoriaRepo.deleteById(id);
		}
		catch (DataIntegrityViolationException e){
			throw new DataIntegrityException("não é possivel excluir uma categoria que possui produtos");
		}
		
	}

	public List<Categoria> findAll() {
		// TODO Auto-generated method stub
		return categoriaRepo.findAll();
	}
	
	public Page<Categoria> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		
		PageRequest pageRequest = PageRequest.of(page, linesPerPage,Direction.valueOf(direction), orderBy);
		return categoriaRepo.findAll(pageRequest);
	}
	
	public Categoria fromDTO(CategoriaDTO categoria) {
		return new Categoria(categoria.getId(), categoria.getNome());
	}
	
	private void updateData(Categoria newObj, Categoria categoria) {
		

		newObj.setNome(categoria.getNome());
	}


}
