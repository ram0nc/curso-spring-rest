package com.ramon.cursomc.services;

import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ramon.cursomc.domain.ItemPedido;
import com.ramon.cursomc.domain.PagamentoComBoleto;
import com.ramon.cursomc.domain.Pedido;
import com.ramon.cursomc.domain.enums.EstadoPagamento;
import com.ramon.cursomc.repository.ItemPedidoRepository;
import com.ramon.cursomc.repository.PagamentoRepository;
import com.ramon.cursomc.repository.PedidoRepository;
import com.ramon.cursomc.resources.BoletoService;
import com.ramon.cursomc.services.exceptions.ObjectNotFoundException;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository pedidoRepo;
	
	@Autowired
	private BoletoService boletoService;
	
	@Autowired
	private ItemPedidoRepository itemPedidoRepository;
	
	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private ClienteService clienteService;
	public Pedido find(Integer id) {
		Optional<Pedido> pedido = pedidoRepo.findById(id);
	
		return pedido.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Pedido.class.getName()));
	}

	@Transactional
	public @Valid Pedido insert(@Valid Pedido obj) {
		
		obj.setId(null);
		obj.setInstate(new Date());
		obj.setCliente(clienteService.find(obj.getCliente().getId()));
		obj.getPagamento().setEstado(EstadoPagamento.PENDENTE);
		obj.getPagamento().setPedido(obj);
		if(obj.getPagamento() instanceof PagamentoComBoleto) {
			PagamentoComBoleto pagmto = (PagamentoComBoleto) obj.getPagamento();

			boletoService.preencherPagamentoComBoleto(pagmto, obj.getInstate());
		}
		obj = pedidoRepo.save(obj);
		pagamentoRepository.save(obj.getPagamento());
		for(ItemPedido ip : obj.getItens()) {
			ip.setDesconto(0.0);
			ip.setProduto(produtoService.find(ip.getProduto().getId()));
			ip.setPreco(produtoService.find(ip.getProduto().getId()).getPreco());
			ip.setPedido(obj);
		}
		itemPedidoRepository.saveAll(obj.getItens());
		emailService.sendOrderConfirmationHtmlEmail(obj);
		return obj;
	}
}
